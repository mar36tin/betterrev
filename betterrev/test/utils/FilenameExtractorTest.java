package utils;

import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static utils.FilenameExtractor.*;

/**
 * The unit tests below are using the Given..When..Then style,
 * and hence each step (block of code) is separated by a blank line.
 *
 */
public class FilenameExtractorTest {

   private static final String EMPTY_STRING = "";

   @Test
   public void should_return_an_empty_list_when_input_diff_string_is_null() {
        Set<String> actualFilesList = extractFilenamesFromPullRequestDiffText(null);

        assertThat("An empty set has not been returned.", actualFilesList, is(equalTo(EMPTY_FILES_LIST)));
   }

    @Test
    public void should_return_an_empty_list_when_input_diff_string_is_empty() {
        Set<String> actualFilesList = extractFilenamesFromPullRequestDiffText(EMPTY_STRING);

        assertThat("An empty set has not been returned.", actualFilesList, is(equalTo(EMPTY_FILES_LIST)));
    }

    @Test
    public void should_return_an_empty_list_for_non_empty_diff_string_containing_no_files() {
        String someDiffString =
                "diff -r 6e9df89ba172268db2ff815caee67cc1c70298b1 -r 218c7ad8deb066b652970cb69bd8256cd7827f44 new_file\n" +
                "@@ -0,0 +1,1 @@\n" +
                "+some new code";

        Set<String> actualFilesList = extractFilenamesFromPullRequestDiffText(someDiffString);

        assertThat("An empty set has not been returned.", actualFilesList, is(equalTo(EMPTY_FILES_LIST)));
    }

    @Test
    public void should_return_one_file_for_non_empty_valid_diff_string_containing_one_file_change() {
        String someDiffString =
                "diff -r 6e9df89ba172268db2ff815caee67cc1c70298b1 -r 218c7ad8deb066b652970cb69bd8256cd7827f44 new_file\n" +
                        "--- /dev/null\n" +
                        "+++ b/new_file\n" +
                        "@@ -0,0 +1,1 @@\n" +
                        "+some new code";

        Set<String> actualFilesList = extractFilenamesFromPullRequestDiffText(someDiffString);
        Set<String> ONE_FILE_LIST = new HashSet<>();
        ONE_FILE_LIST.add("new_file");

        assertThat("A set with at least one file has not been returned.", actualFilesList, is(equalTo(ONE_FILE_LIST)));
    }
}